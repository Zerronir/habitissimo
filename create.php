<?php
// Incluimos los archivos que necesitamos para obetner datos
// We'll include the files we need to get data

include ("api/Classes/Request.php");
include ("api/Classes/Users.php");
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <header>

    </header>

    <main>
        <section>
            <article>
                <form action="api/createRequest.php" method="post">

                    <label for="requestTitle">Título:</label>
                    <input type="text" id="requestTitle" name="requestTitle">

                    <label for="requestDescription">Descripción de la solicitud:</label>
                    <textarea name="requestDescription" id="requestDescription" cols="30" rows="10"></textarea>

                    <label for="requestCategory">Categoría:</label>
                    <select name="requestCategory" id="requestCategory">
                        <option value="">-- Seleccione una categoría --</option>
                        <?php include ("api/optionList.php"); ?>
                    </select>

                    <label for="requestEmail">Correo electrónico:</label>
                    <input type="email" id="requestEmail" name="requestEmail" required>

                    <label for="requestPhone">Teléfono:</label>
                    <input type="text" id="requestPhone" name="requestPhone">

                    <label for="requestAddress">Dirección</label>
                    <input type="text" id="requestAddress" name="requestAddress">

                    <input type="submit" name="Enviar">

                </form>
            </article>
        </section>
    </main>

</body>
</html>
