DROP DATABASE IF EXISTS habitissimo_test;

CREATE DATABASE habitissimo_test;
USE habitissimo_test;

/*- Tabla para los usuarios -*/
CREATE TABLE IF NOT EXISTS users (
	userId int not null auto_increment,
    userMail varchar(60) not null,
    userPhone int(9) not null,
    userAddress varchar (150) not null,
    PRIMARY KEY (userId)
);

CREATE TABLE IF NOT EXISTS categories (
	categoryId int not null auto_increment,
    categoryName varchar(50),
    PRIMARY KEY (categoryId)
);

INSERT INTO categories (categoryName) VALUES ('Kitchen');
INSERT INTO categories (categoryName) VALUES ('Bathroom');
INSERT INTO categories (categoryName) VALUES ('Living_Room');
INSERT INTO categories (categoryName) VALUES ('General');
INSERT INTO categories (categoryName) VALUES ('Bedroom');

/*- Tabla para los presupuestos -*/
CREATE TABLE IF NOT EXISTS request (
	requestId int not null auto_increment,
    requestTitle varchar(30) not null,
    requestDescription text not null,
    requestCategory int not null,
    requestStatus enum('Pending', 'Published', 'Rejected') not null,
    requestedBy int not null,
    PRIMARY KEY (requestId),
    FOREIGN KEY (requestedBy) REFERENCES users(userId),
    FOREIGN KEY (requestCategory) REFERENCES categories(categoryId)
);

select * from categories;
select * from request;


SELECT A.requestTitle, A.requestStatus, B.userMail, C.categoryName FROM request A LEFT JOIN users B ON A.requestBy = B.userId AND RIGHT JOIN categories C ON A.requestCategory = C.categoryId;
