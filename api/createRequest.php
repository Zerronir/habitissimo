<?php
include ("Classes/Request.php");
include ("Classes/Users.php");

$requestTitle = $_REQUEST["requestTitle"];
$requestDescription = $_REQUEST["requestDescription"];
$requestEmail = $_REQUEST["requestEmail"];
$requestCategory = $_REQUEST["requestCategory"];
$requestAddress = $_REQUEST["requestAddress"];
$requestPhone = $_REQUEST["requestPhone"];

if(!filter_var($requestEmail, FILTER_VALIDATE_EMAIL)){
    header ("Location: ../create.php?error=1");
}

// Montamos la array que enviaremos a la api
// Setting our array to send
$jsonData = array(
    'requestTitle' => $requestTitle,
    'requestDescription' => $requestDescription,
    'requestCategory' => $requestEmail,
    'requestStatus' => 'Pending'
);

$jsonRequest = json_encode($jsonData);

// Buscamos si el usuario está registrado, en ese caso solo modificaremos los datos del usuario
// We'll get the user data, if it is not false, we'll update phone and address
if (Users::getUser($requestEmail) != false){

    // Montamos otro json para hacer un update del usuario
    // We set up a new json to update the user da
    if(Users::modifyUser($requestPhone, $requestAddress) == true){
        try {
            Users::getUser($requestEmail);
            Request::createRequest($jsonRequest);
        }catch (\Exception $err){
            print "No se pudo ejecutar la acción por el siguiente motivo: <br>".$err."<br> Por favor, póngase en contacto con un administrador, gracias";
        }

    }

}else {
    // Creamos un json para crear un nuevo usuario
    // Set up a new json to create an user for the costumer
    $jsonUserData = array(
        'userMail' => $requestEmail,
        'userPhone' => $requestPhone,
        'userAddress' => $requestAddress
    );

    $jsonUser = json_encode($jsonUserData);

    // Primero creamos el usuario
    // First we must create the user
    if(Users::newUser($jsonUser) != false){

        try{
            // Creamos la solicitud de presupuesto en base a lo que el usuario nos pide
            // We send the request with all the user's input
            Request::createRequest($jsonRequest);

        }catch (\Exception $err){
            print $err;
        }

    }


}
?>