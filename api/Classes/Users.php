<?php

// Creamos la clase Users para poder gestionar el alta o modificación de clientes
// Users's class is made to register or modify costumers
class Users{

    // Creamos el enlace a la bbdd
    // Setting up the db link
    public function dbConn (){
        $dbUser = "root";
        $dbPass = "";
        $dbName = "habitissimo_test";
        $dbHost = "localhost";
        $dbPort = "3306";

        $link = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName, $dbPort);

        if(!$link) {
            echo "Error Connecting to Database";
        }else{
            return $link;
        }
        return $link;
    }

    // This function is the first we'll use to find the user
    // Esta clase es la primera función que usaremos para poder encontrar a un usuario
    public static function getUser($costumerMail){

    }

    // This function is made to register new costumers after a sucsessfull request
    // Esta función servirá para crear nuevos usuarios al crear una solicitud correctamente
    public static function newUser($jsonUser){

        $link = (new Users)->dbConn();

        $userData = (json_decode($jsonUser, true));

        $createUser = "INSERT INTO users (userMail, userPhone, userAddress) VALUES ('".$userData["userMail"]."', '".$userData["userPhone"]."', '".$userData["userAddress"]."')";
        $user = mysqli_query($link, $createUser);

        if(!$user){
            return mysqli_error($link);
        }

        return true;

    }

    // This function is made to modify costumer's data if is already registered on the system
    // Esta función nos servirá para modificar los datos de un cliente en el sistema
    public static function modifyUser($costumerPhone, $costumerAddress){

    }


}