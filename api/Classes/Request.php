<?php

// Request class is made to manage the requests made by costumers
// This class allows us to manage new request and modify older or mistaken requests
// Creamos una clase para poder gestionar las solicitudes de presupuesto
// Esta clase nos permite gestionar las nuevas peticiones y modificar peticiones existentes

class Request{

    // Creamos la conexión a la base de datos
    // Creating database connection
    public function dbConn (){
        $dbUser = "root";
        $dbPass = "";
        $dbName = "habitissimo_test";
        $dbHost = "localhost";
        $dbPort = "3306";

        $link = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName, $dbPort);

        if(!$link) {
            echo "Error Connecting to Database";
        }else{
            return $link;
        }
        return $link;
    }

    // Necesitamos esta función para poder obtener las categorías
    // This is the function to list all our categories
    public static function listCategories(){

        // Enlace a la base de datos
        // Link to connect to database
        $link = (new Request)->dbConn();

        try {

            // Seleccionamos los datos de la base de datos
            // Getting data from database
            $selectCategories = "SELECT categoryId, categoryName FROM categories";
            $getCategories = mysqli_query($link, $selectCategories);

            // Comprobamos que el resultado no sea false
            // Checking we got a successful connection
            if(!$getCategories){
                print mysqli_error($link);
            }

            $jsonEncode = array();
            // Si tenemos una buena conexión con la base de datos devolveremos el resultado
            // In case our connection to database is successfull we'll return all requested data
            while($categoryRow = mysqli_fetch_array($getCategories)){

                $categoryData = array(
                    "categoryId" => $categoryRow[0],
                    "categoryName" => $categoryRow[1]
                );
                array_push($jsonEncode, $categoryData);
            }

            $catData = json_encode($jsonEncode);
            return $catData;


        }catch (\Exception $e){
            print $e;
        }

    }

    // Creamos una función para poder crear la solicitud de presupuesto
    // We create a static function to create new requests from the web
    public static function createRequest($jsonRequest){
        // Creamos el enlace a la base de datos
        // We create de database link
        $link = (new Request)->dbConn();

        $createRequest = (json_decode($jsonRequest, true));

        //$insertRequest = "INSERT INTO request ()";

    }

    // Creamos una función para poder modificar alguna solicitud
    // Here's another static function to allow costumers to modify older or mistaken
    // requests
    public static function modifyRequest(){

    }

    // Esta función la usaremos para poder descartar/eliminar una solicitud de presupuesto
    // We'll use this function whenever the user wants to delete/discard a request
    public static function rejectRequest(){

    }

    // Esta función sirve para publicar las peticiones de presupuesto
    // This function is made to publish any request from the user
    public static function publishRequest(){

    }
}