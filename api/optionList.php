<?php
    $listCategories = Request::listCategories();
    $decode = (json_decode($listCategories, true));

    // Listamos cada categoría en una lista
    // Listing all categories
    foreach($decode as $data){
        $catId = $data['categoryId'];
        $catName = str_replace("_", " ", $data['categoryName']);
        ?>
        <option value="<?=$catId;?>">-- <?=$catName;?> --</option>
    <?}?>